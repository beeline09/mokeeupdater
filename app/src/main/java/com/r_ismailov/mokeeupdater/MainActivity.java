package com.r_ismailov.mokeeupdater;

import android.app.Notification;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.FrameLayout;

public class MainActivity extends AppCompatActivity {


    private boolean isFullWait = false;
    private String WaitStringUri = "";
    private String WaitStringName = "";
    private boolean isOTAWait = false;

    Bundle state;

    FrameLayout frMain;

    BroadcastReceiver receiver;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        frMain = (FrameLayout) findViewById(R.id.frMain);

        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Log.e("ACTION", intent.getAction());
                //NotificationManagerCompat.from(MainActivity.this).cancelAll();
            }
        };
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("myAction");
        intentFilter.addAction("action_1");
        registerReceiver(receiver, intentFilter);





        if (state == null) {
            android.support.v4.app.FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.add(R.id.frMain, MainFragment.newInstance("Весь список"));
            transaction.commit();
        }
        processIntent(getIntent());
    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);
        if (state != null) outState = state;
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        state = savedInstanceState;
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        processIntent(intent);
    }

    @Override
    protected void onPause() {
        super.onPause();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(receiver);
    }

    @Override
    protected void onResume() {
        super.onResume();

        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Log.e("ACTION", intent.getAction());
                //NotificationManagerCompat.from(MainActivity.this).cancelAll();
            }
        };
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("myAction");
        intentFilter.addAction("action_1");
        registerReceiver(receiver, intentFilter);

    }

    public void setSubTitle(String subTitle) {
        getSupportActionBar().setSubtitle(subTitle);
    }

    public static void goToFragment(AppCompatActivity act, int FrameLayputResId, Fragment fragment, boolean addToBackStack, String backStackString) {
        android.support.v4.app.FragmentTransaction transaction = act.getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.enter_p, R.anim.exit_p);
        transaction.replace(FrameLayputResId, fragment);
        if (addToBackStack) transaction.addToBackStack(backStackString);
        transaction.commit();
    }

    boolean isMaxSetted = false;
    Notification notification;
    int lastProgress = 0;


    private void processIntent(Intent intent) {
        //get your extras
        if (intent.getExtras() != null) {
            Log.e("EXTRAS NOT NULL", "");
            int fff = intent.getExtras().getInt("showProgress", -1);
            Log.e("EXTRAS SHOW_PROGRESS", String.format("%d", fff));
            if (fff > 0) {
                if (mainActivityListener != null) mainActivityListener.needShowProgressDialog();
            }
        } else {
            Log.e("EXTRAS IS NULL", "");
        }
    }


    public interface MainActivityListener {
        void onResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults);

        void needShowProgressDialog();
    }

    private MainActivityListener mainActivityListener;

    public void setMainActivityListener(MainActivityListener listener) {
        this.mainActivityListener = listener;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (mainActivityListener != null)
            mainActivityListener.onResult(requestCode, permissions, grantResults);


    }


}
