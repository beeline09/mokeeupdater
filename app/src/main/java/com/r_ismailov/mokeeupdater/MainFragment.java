package com.r_ismailov.mokeeupdater;

import android.Manifest;
import android.app.IntentService;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.NotificationCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.FileAsyncHttpResponseHandler;

import org.apache.commons.io.FileUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cz.msebera.android.httpclient.Header;

public class MainFragment extends Fragment {

    private RecyclerView rvMain;
    private MainAdapter adapter;
    private ArrayList<Firmware> items;

    private boolean isFullWait = false;
    private String WaitStringUri = "";
    private String WaitStringName = "";
    private String WaitStringBuildType = "";
    private boolean isOTAWait = false;

    private View v;
    private String title;

    private MainActivity ma;

    private String platformVersion = null;

    public static final int NOTIFICATION_ID = 1;

    public static final String ACTION_1 = "action_1";

    private SharedPreferences sp;

    private MainActivity getAct() {
        if (ma == null) ma = (MainActivity) getActivity();
        return ma;
    }

    public static MainFragment newInstance(String title) {
        MainFragment fragment = new MainFragment();
        Bundle bundle = new Bundle();
        bundle.putString("title", title);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            title = getArguments().getString("title", "");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        v = inflater.inflate(R.layout.main_fr, container, false);

        sp = PreferenceManager.getDefaultSharedPreferences(getAct());

        setRetainInstance(true);
        setHasOptionsMenu(true);

        getAct().setSubTitle(title);

        rvMain = (RecyclerView) v.findViewById(R.id.rvMain);

        items = new ArrayList<>();


        adapter = new MainAdapter(getAct(), items);

        adapter.setAdapterListener(new MainAdapter.AdapterListener() {
            @Override
            public void onDownloadFullClick(String url, String fileName) {
                if (PermissionsHelper.isPermissionGranted(getAct(), Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    downloadFull(url, fileName);
                } else {
                    PermissionsHelper.requestPermission(getAct(), Manifest.permission.WRITE_EXTERNAL_STORAGE, 7);
                    isFullWait = true;
                    WaitStringUri = url;
                    WaitStringName = fileName;
                }
            }

            @Override
            public void onDownloadOTAClick(String url, String fileName, String buildType) {
                if (PermissionsHelper.isPermissionGranted(getAct(), Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    downloadOTA(url, fileName, buildType);
                } else {
                    PermissionsHelper.requestPermission(getAct(), Manifest.permission.WRITE_EXTERNAL_STORAGE, 8);
                    isOTAWait = true;
                    WaitStringUri = url;
                    WaitStringName = fileName;
                    WaitStringBuildType = buildType;
                }
            }
        });

        RecyclerView.LayoutManager lm = new LinearLayoutManager(getAct());

        rvMain.setLayoutManager(lm);
        rvMain.setAdapter(adapter);
        RecyclerView.ItemAnimator itemAnimator = new DefaultItemAnimator();
        rvMain.setItemAnimator(itemAnimator);


        //String vers = System.getProperty("ro.mk.version", "-1111");

        getAct().setMainActivityListener(new MainActivity.MainActivityListener() {
            @Override
            public void onResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
                if (requestCode == 7) {
                    if (grantResults.length > 0
                            && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        isFullWait = false;
                        downloadFull(WaitStringUri, WaitStringName);
                        // permission was granted, yay! Do the
                        // contacts-related task you need to do.

                    } else {

                        // permission denied, boo! Disable the
                        // functionality that depends on this permission.
                    }
                } else if (requestCode == 8) {
                    if (grantResults.length > 0
                            && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        isOTAWait = false;
                        downloadOTA(WaitStringUri, WaitStringName, WaitStringBuildType);
                        // permission was granted, yay! Do the
                        // contacts-related task you need to do.

                    } else {

                        // permission denied, boo! Disable the
                        // functionality that depends on this permission.
                    }
                }
            }

            @Override
            public void needShowProgressDialog() {
                if (pd != null) pd.show();
            }
        });


        loadAllData();

        return v;
    }


    boolean isMaxSetted = false;
    Notification notification;
    int lastProgress = 0;
    MyProgressDialog pd;

    private void processIntent(Intent intent) {
        //get your extras
        if (intent.getExtras() != null) {
            Log.e("EXTRAS NOT NULL", "");
            int fff = intent.getExtras().getInt("showProgress", -1);
            Log.e("EXTRAS SHOW_PROGRESS", String.format("%d", fff));
            if (fff > 0) {
                if (pd != null) pd.show();
            }
        } else {
            Log.e("EXTRAS IS NULL", "");
        }
    }

    AsyncHttpClient ahc;
    boolean isAHCCancelled = false;


    private void downloadFull(String url, String fileName) {

        if (ahc != null && !isAHCCancelled) {
            if (pd != null) pd.show();
        } else {


            Intent action1Intent = new Intent(getAct(), NotificationActionService.class)
                    .setAction(ACTION_1);

            PendingIntent action1PendingIntent = PendingIntent.getService(getAct(), 0,
                    action1Intent, PendingIntent.FLAG_ONE_SHOT);

            final NotificationCompat.Builder notificationBuilder =
                    (NotificationCompat.Builder) new NotificationCompat.Builder(getAct())
                            .setSmallIcon(R.drawable.ic_notif_logo)
                            .setContentTitle(getString(R.string.app_name))
                            .setContentText("Загрузка полной сборки...")
                            .setProgress(100, 0, false)
                            .setContentIntent(action1PendingIntent)
                            /*.addAction(new NotificationCompat.Action(R.mipmap.ic_launcher,
                                    "Action 1", action1PendingIntent))*/;

            final NotificationManagerCompat notificationManager = NotificationManagerCompat.from(getAct());
            notificationManager.notify(NOTIFICATION_ID, notificationBuilder.build());


            //Send the notification:
            notification = notificationBuilder.build();
            notificationManager.notify(NOTIFICATION_ID, notification);


            ahc = new AsyncHttpClient();

            pd = new MyProgressDialog(getAct());
            pd.setTitle("Пожалуйста, подождите");
            pd.setMessage("Идет загрузка FULL-прошивки");
            pd.setMax(100);
            pd.setProgress(0);
            pd.setIndeterminate(false);
            pd.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            pd.setButton(DialogInterface.BUTTON_POSITIVE, "Отмена", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    if (ahc != null) ahc.cancelAllRequests(true);
                    notificationManager.cancelAll();
                    if (pd.isShowing()) pd.dismissManually();
                }
            });
            pd.setButton(DialogInterface.BUTTON_NEUTRAL, "Скачать в фоне", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    pd.hide();
                }
            });
            pd.setCancelable(false);
            pd.show();


            ahc.get(getAct(), url, new FileAsyncHttpResponseHandler(getAct()) {
                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, File file) {
                    pd.dismissManually();
                    Snackbar.make(getAct().frMain, "Ошибка загрузки!", Snackbar.LENGTH_LONG).show();
                }

                @Override
                public void onCancel() {
                    super.onCancel();
                    isAHCCancelled = true;
                }

                @Override
                public void onFinish() {
                    super.onFinish();
                    isAHCCancelled = true;
                    notificationManager.cancelAll();
                    if (pd.isShowing()) pd.dismiss();
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, final File file) {
                    if (pd.isShowing()) pd.dismiss();
                    Log.e("FULL_FILE", file.toString());
                    Snackbar.make(getAct().frMain, "Готово!", Snackbar.LENGTH_LONG).setCallback(new Snackbar.Callback() {
                        @Override
                        public void onDismissed(Snackbar snackbar, int event) {
                            super.onDismissed(snackbar, event);

                            AlertDialog.Builder adb = new AlertDialog.Builder(getAct());
                            adb.setMessage("Хотите перезагрузиться в рекавери и прошить данный файл?");
                            adb.setTitle("Внимание, вопрос...");
                            adb.setNegativeButton("Нет", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                            adb.setPositiveButton("Да", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    File fff = Environment.getDataDirectory();
                                    File outFile = new File(fff, "update.zip");
                                    if (!outFile.exists()) {
                                        outFile.mkdirs();
                                    } else {
                                        outFile.delete();
                                    }

                                    try {
                                        outFile.createNewFile();
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }

                                    new CopyFile(FILE_TYPE_FULL).execute(new File[]{file, outFile});
                                }
                            });
                            adb.show();
                        }

                        @Override
                        public void onShown(Snackbar snackbar) {
                            super.onShown(snackbar);
                        }
                    }).show();
                }

                @Override
                public void onProgress(long bytesWritten, long totalSize) {
                    super.onProgress(bytesWritten, totalSize);

                    if (!isMaxSetted) {
                        pd.setMax((int) totalSize);
                        isMaxSetted = true;
                    }

                    String str = String.format("%.0f " + getByteFormat(bytesWritten) + " из %.0f " + getByteFormat(totalSize), getNumberDouble(bytesWritten), getNumberDouble(totalSize));


                    pd.setProgressNumberFormat(str);


                    pd.setProgress((int) bytesWritten);

                    int current = (int) (bytesWritten * 100 / totalSize);
                    if (lastProgress != current) {
                        //Update notification information:
                        notificationBuilder.setProgress(100, current, false);
                        //Send the notification:
                        notification = notificationBuilder.build();
                        notificationManager.notify(NOTIFICATION_ID, notification);
                        lastProgress = current;
                    }


                }
            });
        }

    }

    private void downloadOTA(String url, String fileName, String buildType) {
        Log.e("OTA_URL", url);

        String dateToUpadte = null;
        String dateFromUpadte = null;

        Pattern pattern = Pattern.compile("\\-?\\d{6,12}\\-");

        Matcher matcher = pattern.matcher(fileName);
        if (matcher.find()) {

            dateToUpadte = matcher.group(0).substring(1, matcher.group(0).length() - 1);

            Log.e("MATCH_TO", dateToUpadte);
        }


        Process p = null;
        String board_platform = "";
        try {
            p = new ProcessBuilder("/system/bin/getprop", "ro.mk.version").redirectErrorStream(true).start();
            BufferedReader br = new BufferedReader(new InputStreamReader(p.getInputStream()));
            String line = "";
            while ((line = br.readLine()) != null) {
                board_platform = line;
            }
            p.destroy();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        platformVersion = board_platform;

        Matcher matcher1 = pattern.matcher(platformVersion);
        if (matcher1.find()) {

            dateFromUpadte = matcher1.group(0).substring(1, matcher1.group(0).length() - 1);

            Log.e("MATCH_FROM", dateFromUpadte);
        }

        String plat = fileName.substring(0, fileName.indexOf("-" + sp.getString("devCode", "klteduos")));

        if (dateFromUpadte != null && dateFromUpadte.length() > 0 && dateToUpadte != null && dateToUpadte.length() > 0) {
            String resss = "OTA-" + plat + "-" + sp.getString("devCode", "klteduos") + "-" + dateFromUpadte + "-" + dateToUpadte + "-" + buildType + ".zip";
            Log.e("OTA_FILE_NAME", resss);

            if (pd1 != null) {
                pd1.show();
            } else
                new ParseOTAHTML().execute(url, resss);

        }


    }

    private String getByteFormat(long value) {
        String result = "Байт";
        if (value < 1024) {
            result = "Байт";
        } else if (value >= 1024 && value < 1024000) {
            result = "КБайт";
        } else if (value >= 1024000 && value < 1024000000) {
            result = "МБайт";
        } else if (value >= 1024000000) {
            result = "ГБайт";
        }
        return result;
    }

    private float getNumberDouble(long value) {
        float result = 0;
        if (value < 1024) {
            result = value;
        } else if (value >= 1024 && value < 1024000) {
            result = value / 1024;
        } else if (value >= 1024000 && value < 1024000000) {
            result = value / 1024000;
        } else if (value >= 1024000000) {
            result = value / 1024000000;
        }
        return result;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == 7) {
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                isFullWait = false;
                downloadFull(WaitStringUri, WaitStringName);
                // permission was granted, yay! Do the
                // contacts-related task you need to do.

            } else {

                // permission denied, boo! Disable the
                // functionality that depends on this permission.
            }
        } else if (requestCode == 8) {
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                isOTAWait = false;
                downloadOTA(WaitStringUri, WaitStringName, WaitStringBuildType);
                // permission was granted, yay! Do the
                // contacts-related task you need to do.

            } else {

                // permission denied, boo! Disable the
                // functionality that depends on this permission.
            }
        }

    }

    MyProgressDialog pd1;

    private class ParseOTAHTML extends AsyncTask<String, Void, String> {

        Document doc;

        Elements mainHeaderElements;


        List<Firmware> items1;

        boolean isMaxSetted = false;
        Notification notification;
        int lastProgress = 0;
        MyProgressDialog pd;
        AsyncHttpClient asyncHttpClient;

        String url = null;
        String needFileName = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            items1 = new ArrayList<>();

            pd1 = new MyProgressDialog(getAct());
            pd1.setTitle("Подождите");
            pd1.setMessage("Идет поиск ОТА-обновления для вашей текущей сборки");
            pd1.setIndeterminate(true);
            pd1.setCancelable(false);
            pd1.setButton(DialogInterface.BUTTON_NEGATIVE, "Отмена", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    ParseOTAHTML.this.cancel(true);
                    if (pd1 != null) pd1.dismissManually();
                    pd1 = null;
                }
            });
            pd1.show();

        }

        @Override
        protected String doInBackground(String... params) {

            url = params[0];
            needFileName = params[1];

            try {
                doc = Jsoup.connect(url).get();
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (doc != null) {
                mainHeaderElements = doc.select("table.table-bordered");

                Elements tbodyElements = mainHeaderElements.select("tbody");
                Log.e("SIZE1", String.format("%d", tbodyElements.size()));

                for (int i = 0; i < tbodyElements.size(); i++) {
                    Elements elTR = tbodyElements.select("tr");
                    //Log.e("TR1", elTR.toString());
                    Log.e("TR1SIZE1", String.format("%d", elTR.size()));

                    for (int k = 0; k < elTR.size(); k++) {
                        Element elItem = elTR.get(k);
                        Log.e("elITEM", elItem.toString());

                        Log.e("elITEMsSIZE", String.format("%d", elItem.getElementsByTag("td").size()));

                        Elements elSubItem = elItem.getElementsByTag("td");

                        Firmware firmwareItem = new Firmware();

                        for (int n = 0; n < elSubItem.size(); n++) {
                            Elements elll = elSubItem.get(n).getAllElements();
                            if (n == 0) {
                                String devCode = elll.get(0).getElementsByTag("a").html();
                                Log.e("devCode = ", devCode);
                                firmwareItem.setDeviceCode(devCode);
                            } else if (n == 1) {
                                String firmWareType = elll.get(0).html();
                                Log.e("firmwareType = ", firmWareType);
                                firmwareItem.setFirmwareType(firmWareType);
                            } else if (n == 2) {
                                String downLinkFull = elll.get(0).getElementsByAttribute("href").get(0).attr("href");
                                String fileNameFull = elll.get(0).getElementsByAttribute("href").get(0).html();
                                Log.e("FULL = ", downLinkFull);
                                firmwareItem.setDownloadFull(downLinkFull);
                                firmwareItem.setFileNameFULL(fileNameFull);
                            } else if (n == 3) {
                                String size = elll.get(0).html();
                                Log.e("FULL_SIZE = ", size);
                                firmwareItem.setFileSize(size);
                            } else if (n == 4) {
                                String downloadCount = elll.get(0).html();
                                Log.e("DOWNLOAD_COUNT = ", downloadCount);
                                firmwareItem.setDownloadCount(downloadCount);
                            } else if (n == 5) {
                                String dateCreate = elll.get(0).html();
                                Log.e("DATE = ", dateCreate);
                                firmwareItem.setDateAdded(dateCreate);
                            }
                        }
                        items1.add(firmwareItem);

                    }

/*
                        Elements devName = elTR.select("td");
                        Log.e("TD1", devName.toString());
                        Log.e("TD1SIZE1", String.format("%d", devName.size()));*/
                }
            }


            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            pd1.dismissManually();

            if (items1.size() > 0) {

                for (Firmware item : items1) {
                    Log.e("OTA_ITEM_NAME", item.getFileNameFULL());
                    Log.e("OTA_ITEM_LINK", item.getDownloadFull());

                    if (item.getFileNameFULL().contains(needFileName)) {
                        Log.e("УРАААА", "Ес!!!");

                        if (ahc != null && !isAHCCancelled) {
                            if (pd1 != null) pd1.show();
                        } else {


                            Intent action1Intent = new Intent(getAct(), NotificationActionService.class)
                                    .setAction(ACTION_1);

                            PendingIntent action1PendingIntent = PendingIntent.getService(getAct(), 0,
                                    action1Intent, PendingIntent.FLAG_ONE_SHOT);

                            final NotificationCompat.Builder notificationBuilder =
                                    (NotificationCompat.Builder) new NotificationCompat.Builder(getAct())
                                            .setSmallIcon(R.drawable.ic_notif_logo)
                                            .setContentTitle(getString(R.string.app_name))
                                            .setContentText("Загрузка ОТА обновления...")
                                            .setProgress(100, 0, false)
                                            .setContentIntent(action1PendingIntent)
                            /*.addAction(new NotificationCompat.Action(R.mipmap.ic_launcher,
                                    "Action 1", action1PendingIntent))*/;

                            final NotificationManagerCompat notificationManager = NotificationManagerCompat.from(getAct());
                            notificationManager.notify(NOTIFICATION_ID, notificationBuilder.build());


                            //Send the notification:
                            notification = notificationBuilder.build();
                            notificationManager.notify(NOTIFICATION_ID, notification);


                            asyncHttpClient = new AsyncHttpClient();

                            pd1 = new MyProgressDialog(getAct());
                            pd1.setTitle("Пожалуйста, подождите");
                            pd1.setMessage("Идет загрузка ОТА-обновления");
                            pd1.setMax(100);
                            pd1.setProgress(0);
                            pd1.setIndeterminate(false);
                            pd1.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                            pd1.setButton(DialogInterface.BUTTON_POSITIVE, "Отмена", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    if (asyncHttpClient != null)
                                        asyncHttpClient.cancelAllRequests(true);
                                    notificationManager.cancelAll();
                                    if (pd1.isShowing()) pd1.dismissManually();
                                    pd1 = null;
                                }
                            });
                            pd1.setButton(DialogInterface.BUTTON_NEUTRAL, "Скачать в фоне", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    pd1.hide();
                                }
                            });
                            pd1.setCancelable(false);
                            pd1.show();


                            final String fileFull = item.getDownloadFull().substring(item.getDownloadFull().indexOf("get=") + 4);
                            String urll = "http://rom.mk/" + fileFull;


                            asyncHttpClient.get(getAct(), urll, new FileAsyncHttpResponseHandler(getAct()) {
                                @Override
                                public void onProgress(long bytesWritten, long totalSize) {
                                    super.onProgress(bytesWritten, totalSize);

                                    pd1.setProgress((int) (bytesWritten * 100 / totalSize));
                                    if (!isMaxSetted) {
                                        pd1.setMax((int) totalSize);
                                        isMaxSetted = true;
                                    }

                                    String str = String.format("%.0f " + getByteFormat(bytesWritten) + " из %.0f " + getByteFormat(totalSize), getNumberDouble(bytesWritten), getNumberDouble(totalSize));


                                    pd1.setProgressNumberFormat(str);


                                    pd1.setProgress((int) bytesWritten);

                                    int current = (int) (bytesWritten * 100 / totalSize);
                                    if (lastProgress != current) {
                                        //Update notification information:
                                        notificationBuilder.setProgress(100, current, false);
                                        //Send the notification:
                                        notification = notificationBuilder.build();
                                        notificationManager.notify(NOTIFICATION_ID, notification);
                                        lastProgress = current;
                                    }
                                }

                                @Override
                                public void onFailure(int statusCode, Header[] headers, Throwable throwable, File file) {

                                }

                                @Override
                                public void onSuccess(int statusCode, Header[] headers, File file) {
                                    Log.e("OTA_FILE_COMPLETE", file.toString());

                                    File sdCard = Environment.getExternalStorageDirectory();

                                    final File sdFile = new File(sdCard, needFileName);

                                    try {
                                        try {
                                            FileUtils.moveFile(file, sdFile);
                                        } catch (Exception e1) {
                                            e1.printStackTrace();
                                        }
                                        Snackbar.make(getAct().frMain, "Файл успешно загружен по пути:\n" + sdFile.toString(), Snackbar.LENGTH_LONG).setCallback(new Snackbar.Callback() {
                                            @Override
                                            public void onDismissed(Snackbar snackbar, int event) {
                                                super.onDismissed(snackbar, event);
                                                AlertDialog.Builder adb = new AlertDialog.Builder(getAct());
                                                adb.setTitle("Обновление загружено");
                                                adb.setMessage("Перезагрузиться в рекавери для обновления?\n\n Zip-файл придется выбрать вручную");
                                                adb.setNegativeButton("Нет", new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialog, int which) {
                                                        dialog.dismiss();
                                                    }
                                                });
                                                adb.setPositiveButton("Да", new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialog, int which) {

                                                        File fff = Environment.getDataDirectory();
                                                        File outFile = new File(fff, "update.zip");
                                                        if (!outFile.exists()) {
                                                            outFile.mkdirs();
                                                        } else {
                                                            outFile.delete();
                                                        }

                                                        try {
                                                            outFile.createNewFile();
                                                        } catch (IOException e) {
                                                            e.printStackTrace();
                                                        }

                                                        new CopyFile(FILE_TYPE_OTA).execute(new File[]{sdFile, outFile});

                                                    }
                                                });
                                                adb.setNegativeButton("Нет", null);
                                                adb.show();
                                            }

                                            @Override
                                            public void onShown(Snackbar snackbar) {
                                                super.onShown(snackbar);
                                            }
                                        }).show();
                                        pd1.dismissManually();
                                        pd1 = null;
                                        file.delete();

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        Log.e("ERROR", e.toString());
                                    }

                                    if (pd1 != null) pd1.dismissManually();
                                    pd1 = null;

                                }
                            });

                        }

                    } else {
                        //Snackbar.make(getAct().frMain, "Нет совместимых ОТА-обновлений.\nСкачайте полную сборку.", Snackbar.LENGTH_LONG).show();
                        //if (pd1 != null) pd1.dismissManually();
                        //pd1 = null;
                    }

                }
            } else {
                Snackbar.make(getAct().frMain, "Не найдено ни одного ОТА-обновления", Snackbar.LENGTH_LONG).show();
                if (pd1 != null) pd1.dismissManually();
                pd1 = null;
            }

        }
    }

    private class ParseHTML extends AsyncTask<String, Void, String> {

        String result = "";
        Elements mainHeaderElements;

        ProgressDialog pd;


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            items.clear();
            adapter.notifyDataSetChanged();
            pd = new ProgressDialog(getAct());
            pd.setTitle("Пожалуйста подождите...");
            pd.setMessage("Загрузка списка");
            pd.setCancelable(false);
            pd.show();
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected String doInBackground(String... params) {

            try {
                Document doc = Jsoup.connect("http://download.mokeedev.com/?device=" + sp.getString("devCode", "klteduos")).get();

                if (doc != null) {
                    mainHeaderElements = doc.select("table.table-bordered");
                    result = mainHeaderElements.toString();

                    Elements tbodyElements = mainHeaderElements.select("tbody");
                    Log.e("SIZE1", String.format("%d", tbodyElements.size()));

                    for (int i = 0; i < tbodyElements.size(); i++) {
                        Elements elTR = tbodyElements.select("tr");
                        //Log.e("TR1", elTR.toString());
                        Log.e("TR1SIZE1", String.format("%d", elTR.size()));

                        for (int k = 0; k < elTR.size(); k++) {
                            Element elItem = elTR.get(k);
                            Log.e("elITEM", elItem.toString());

                            Log.e("elITEMsSIZE", String.format("%d", elItem.getElementsByTag("td").size()));

                            Elements elSubItem = elItem.getElementsByTag("td");

                            Firmware firmwareItem = new Firmware();

                            for (int n = 0; n < elSubItem.size(); n++) {
                                Elements elll = elSubItem.get(n).getAllElements();
                                if (n == 0) {
                                    String devCode = elll.get(0).getElementsByTag("a").html();
                                    Log.e("devCode = ", devCode);
                                    firmwareItem.setDeviceCode(devCode);
                                } else if (n == 1) {
                                    String firmWareType = elll.get(0).html();
                                    Log.e("firmwareType = ", firmWareType);
                                    firmwareItem.setFirmwareType(firmWareType);
                                } else if (n == 2) {
                                    String downLinkFull = elll.get(0).getElementsByAttribute("href").get(0).attr("href");
                                    String fileNameFull = elll.get(0).getElementsByAttribute("href").get(0).html();
                                    String downLinkOTA = elll.get(0).getElementsByAttribute("href").get(1).attr("href");
                                    Log.e("FULL = ", downLinkFull);
                                    Log.e("OTA  = ", downLinkOTA);
                                    firmwareItem.setDownloadFull(downLinkFull);
                                    firmwareItem.setDownloadOTA(downLinkOTA);
                                    firmwareItem.setFileNameFULL(fileNameFull);
                                } else if (n == 3) {
                                    String size = elll.get(0).html();
                                    Log.e("FULL_SIZE = ", size);
                                    firmwareItem.setFileSize(size);
                                } else if (n == 4) {
                                    String downloadCount = elll.get(0).html();
                                    Log.e("DOWNLOAD_COUNT = ", downloadCount);
                                    firmwareItem.setDownloadCount(downloadCount);
                                } else if (n == 5) {
                                    String dateCreate = elll.get(0).html();
                                    Log.e("DATE = ", dateCreate);
                                    firmwareItem.setDateAdded(dateCreate);
                                }
                            }
                            items.add(firmwareItem);

                        }

/*
                        Elements devName = elTR.select("td");
                        Log.e("TD1", devName.toString());
                        Log.e("TD1SIZE1", String.format("%d", devName.size()));*/
                    }


                }

            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            //Log.e("TABLE1", result.toString());
            //Log.e("THEAD1", mainHeaderElements.select("thead").toString());
            adapter.notifyDataSetChanged();
            if (pd.isShowing()) pd.dismiss();
        }
    }

    public class MyProgressDialog extends ProgressDialog {
        public MyProgressDialog(Context context) {
            super(context);
        }

        public MyProgressDialog(Context context, int theme) {
            super(context, theme);
        }

        @Override
        public void dismiss() {
            // do nothing
        }

        public void dismissManually() {
            super.dismiss();
        }
    }

    private void loadAllData() {
        new ParseHTML().execute();
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.main, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.action_refresh) {
            loadAllData();
        } else if (item.getItemId() == R.id.action_get_gapps) {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://opengapps.org/"));
            startActivity(browserIntent);
        } else if (item.getItemId() == R.id.action_get_xposed) {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://forum.xda-developers.com/showthread.php?t=3034811"));
            startActivity(browserIntent);
        } else if (item.getItemId() == R.id.action_bag_tracker) {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://plus.google.com/communities/112433388317801966872/stream/eb8b7deb-142e-420d-b2c3-a691deaa947c/"));
            startActivity(browserIntent);
        } else if (item.getItemId() == R.id.action_open_external) {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("download.mokeedev.com/?device=" + sp.getString("devCode", "klteduos")));
            startActivity(browserIntent);
        } else if (item.getItemId() == R.id.action_change_device) {

            final EditText input = new EditText(getAct());
            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.MATCH_PARENT);
            lp.setMargins(dpToPx(20), 0, dpToPx(20), 0);
            input.setLayoutParams(lp);
            input.setImeOptions(EditorInfo.IME_ACTION_DONE);

            AlertDialog.Builder adb = new AlertDialog.Builder(getAct());
            adb.setTitle("Введите код устройства");
            adb.setMessage("Например, для Samsung G900FD код будет klteduos, а для Sony Xperia Z2 будет sirius.\n\nЕсли оставить поле пустым, то по умолчанию будет выбран Samsung G900FD c кодом klteduos.");
            adb.setView(input);
            adb.setPositiveButton("Принять", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    if (input.getText().toString().length() > 0) {
                        sp.edit().putString("devCode", input.getText().toString()).commit();
                        new ParseHTML().execute();
                    } else {
                        sp.edit().putString("devCode", "klteduos").commit();
                        new ParseHTML().execute();
                    }
                }
            });
            adb.setNegativeButton("Отмена", null);
            adb.show();
        }
        return super.onOptionsItemSelected(item);
    }

    public static class NotificationActionService extends IntentService {
        public NotificationActionService() {
            super(NotificationActionService.class.getSimpleName());
        }

        @Override
        protected void onHandleIntent(Intent intent) {
            String action = intent.getAction();
            Log.e("Notification action: ", action);
            if (ACTION_1.equals(action)) {
                // TODO: handle action 1.
                // If you want to cancel the notification: NotificationManagerCompat.from(this).cancel(NOTIFICATION_ID);
                NotificationManagerCompat.from(this).cancel(NOTIFICATION_ID);

                LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
                getApplicationContext().sendBroadcast(intent);
/*

                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                getApplication().startActivity(i);*/
            }
        }
    }

    public int dpToPx(int dp) {
        DisplayMetrics displayMetrics = getAct().getResources().getDisplayMetrics();
        int px = Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
        return px;
    }


    public interface ProgressListener {
        void onProgress(double current, double full);
    }

    public static long copy(final InputStream input, final OutputStream output, int buffersize,
                            ProgressListener listener) throws IOException {
        final byte[] buffer = new byte[buffersize];
        int n = 0;
        long count = 0;
        while (-1 != (n = input.read(buffer))) {
            output.write(buffer, 0, n);
            count += n;
            listener.onProgress(n, input.available());
        }
        return count;
    }

    private static int FILE_TYPE_FULL = 0;
    private static int FILE_TYPE_OTA = 1;


    private class CopyFile extends AsyncTask<File, Double, Integer> {

        MyProgressDialog mpd;
        File inputFile;
        File outputFile;
        int zipType = -1;

        public CopyFile(int zipType) {
            this.zipType = zipType;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            Log.e("CopyFile", "PreExecute");

            mpd = new MyProgressDialog(getAct());
            mpd.setTitle("Пожалуйста подождите");
            mpd.setMessage("Идет копирование файла");
            mpd.setIndeterminate(false);
            mpd.setMax(100);
            mpd.setCancelable(false);
            mpd.setButton(DialogInterface.BUTTON_NEGATIVE, "Отмена", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    mpd.dismissManually();
                    cancel(true);
                }
            });
            mpd.show();

        }

        @Override
        protected Integer doInBackground(File... params) {

            Log.e("CopyFile", "doInBackground");

            inputFile = params[0];
            outputFile = params[1];

            InputStream is = null;
            try {
                is = new FileInputStream(inputFile);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                Snackbar.make(getAct().frMain, "Ошибка копирования3!", Snackbar.LENGTH_LONG).show();
            }

            OutputStream os = null;
            try {
                os = new FileOutputStream(outputFile);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                Snackbar.make(getAct().frMain, "Ошибка копирования2!", Snackbar.LENGTH_LONG).show();
            }

            try {
                copy(is, os, 1024, new ProgressListener() {
                    @Override
                    public void onProgress(double progress, double full) {
                        publishProgress(progress, full);
                    }
                });
            } catch (IOException e) {
                e.printStackTrace();
                Snackbar.make(getAct().frMain, "Ошибка копирования1!", Snackbar.LENGTH_LONG).show();
            }


            return null;
        }

        @Override
        protected void onProgressUpdate(Double... values) {
            super.onProgressUpdate(values);

            Log.e("CopyFile", String.format("Progress = %f of %f", values[0], values[1]));

            double current = values[0] * 100 / values[1];
            mpd.setProgress((int) current);

        }

        @Override
        protected void onPostExecute(Integer s) {
            super.onPostExecute(s);

            Log.e("CopyFile", "onPostExequte");

            if (mpd != null && mpd.isShowing()) mpd.dismissManually();

            if (s == FILE_TYPE_FULL) {
                /*try {
                    Process su = Runtime.getRuntime().exec("su");
                    DataOutputStream outputStream = new DataOutputStream(su.getOutputStream());

                    outputStream.writeBytes("reboot recovery\n");
                    outputStream.flush();
                    su.waitFor();
                } catch (IOException ee) {
                    //throw new Exception(ee);
                } catch (InterruptedException eee) {
                    //throw new Exception(eeee);
                }*/
            } else if (s == FILE_TYPE_OTA) {

            }
        }
    }


}
