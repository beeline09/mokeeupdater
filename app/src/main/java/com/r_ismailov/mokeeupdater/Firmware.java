package com.r_ismailov.mokeeupdater;

/**
 * Created by ADiGA on 01.02.16.
 */
public class Firmware {

    String DeviceCode;
    String DeviceName;
    String FirmwareType;
    String fileNameFULL;
    String DownloadFull;
    String DownloadOTA;
    String FileSize;
    String DownloadCount;
    String DateAdded;

    public Firmware(String deviceCode, String deviceName, String firmwareType, String downloadFull, String downloadOTA, String fileSize, String downloadCount, String dateAdded, String fileNameFull) {
        DeviceCode = deviceCode;
        DeviceName = deviceName;
        FirmwareType = firmwareType;
        DownloadFull = downloadFull;
        DownloadOTA = downloadOTA;
        FileSize = fileSize;
        DownloadCount = downloadCount;
        DateAdded = dateAdded;
        this.fileNameFULL = fileNameFull;
    }

    public Firmware(){}

    public String getFileNameFULL() {
        return fileNameFULL;
    }

    public void setFileNameFULL(String fileNameFULL) {
        this.fileNameFULL = fileNameFULL;
    }

    public String getDeviceCode() {

        return DeviceCode;
    }

    public void setDeviceCode(String deviceCode) {
        DeviceCode = deviceCode;
    }

    public String getDeviceName() {
        return DeviceName;
    }

    public void setDeviceName(String deviceName) {
        DeviceName = deviceName;
    }

    public String getFirmwareType() {
        return FirmwareType;
    }

    public void setFirmwareType(String firmwareType) {
        FirmwareType = firmwareType;
    }

    public String getDownloadFull() {
        return DownloadFull;
    }

    public void setDownloadFull(String downloadFull) {
        DownloadFull = downloadFull;
    }

    public String getDownloadOTA() {
        return DownloadOTA;
    }

    public void setDownloadOTA(String downloadOTA) {
        DownloadOTA = downloadOTA;
    }

    public String getFileSize() {
        return FileSize;
    }

    public void setFileSize(String fileSize) {
        FileSize = fileSize;
    }

    public String getDownloadCount() {
        return DownloadCount;
    }

    public void setDownloadCount(String downloadCount) {
        DownloadCount = downloadCount;
    }

    public String getDateAdded() {
        return DateAdded;
    }

    public void setDateAdded(String dateAdded) {
        DateAdded = dateAdded;
    }
}
