package com.r_ismailov.mokeeupdater;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by ADiGA on 18.11.15.
 */


/**
 * Класс адаптера наследуется от RecyclerView.Adapter с указанием класса, который будет хранить ссылки на виджеты элемента списка, т.е. класса, имплементирующего ViewHolder. В нашем случае класс объявлен внутри класса адаптера.
 */
public class MainAdapter extends RecyclerView.Adapter<MainAdapter.ViewHolder> {

    private List<Firmware> records;
    private AppCompatActivity ctx;
    private String platformVersion = "";
    private SharedPreferences sp;

    int lastPosition = 0;

    public interface AdapterListener{
        void onDownloadFullClick(String url, String fileName);
        void onDownloadOTAClick(String url, String fileName, String buildType);
    }

    private AdapterListener listener;
    public void setAdapterListener(AdapterListener listener){
        this.listener = listener;
    }


    public MainAdapter(AppCompatActivity ctx, List<Firmware> records) {
        this.records = records;
        this.ctx = ctx;

        sp = PreferenceManager.getDefaultSharedPreferences(ctx);

        Process p = null;
        String board_platform = "";
        try {
            p = new ProcessBuilder("/system/bin/getprop", "ro.mk.version").redirectErrorStream(true).start();
            BufferedReader br = new BufferedReader(new InputStreamReader(p.getInputStream()));
            String line = "";
            while ((line = br.readLine()) != null) {
                board_platform = line;
            }
            p.destroy();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        platformVersion = board_platform;
        Log.e("MyVersion", board_platform);

    }


    /**
     * Создание новых View и ViewHolder элемента списка, которые впоследствии могут переиспользоваться.
     */
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.main_item, viewGroup, false);

        ViewHolder holder = new ViewHolder(v);
        holder.itemView.setTag(holder);

        return holder;
    }

    /**
     * Заполнение виджетов View данными из элемента списка с номером i
     */
    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int i) {

        viewHolder.itemView.setTag(i);


        final Firmware record = records.get(i);


        String dateString = record.getDateAdded();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
        Date convertedDate = null;
        try {
            convertedDate = dateFormat.parse(dateString);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        if (convertedDate != null) {
//            App.Log("DATE", convertedDate.toString());
            Calendar cal = Calendar.getInstance(Locale.US);
            Calendar calMy = Calendar.getInstance(Locale.getDefault());
            //cal.setTimeZone(TimeZone.getTimeZone("GMT+08:00"));
            cal.setTime(convertedDate);

            Calendar my = Calendar.getInstance(Locale.US);
            int myOffset = my.getTimeZone().getRawOffset();
            int serverOffset = TimeZone.getTimeZone("GMT+8").getRawOffset();
            int offset = myOffset-serverOffset;
            cal.add(Calendar.MILLISECOND, offset);

            Log.e("Offset", String.format("my = %d, server = %d", myOffset, serverOffset));

            int year = cal.get(Calendar.YEAR);
            int month = cal.get(Calendar.MONTH);
            int day = cal.get(Calendar.DAY_OF_MONTH);

            int hour = cal.get(Calendar.HOUR_OF_DAY);
            int minute = cal.get(Calendar.MINUTE);

            String[] monthShort = ctx.getResources().getStringArray(R.array.months_short);

            if (year == calMy.get(Calendar.YEAR) && month == calMy.get(Calendar.MONTH) && day == calMy.get(Calendar.DAY_OF_MONTH)) {
                viewHolder.tvDateCreated.setText(String.format("Сегодня в %02d:%02d", hour, minute));
            } else if (year == calMy.get(Calendar.YEAR) && month == calMy.get(Calendar.MONTH) && calMy.get(Calendar.DAY_OF_MONTH) - day == 1) {
                viewHolder.tvDateCreated.setText(String.format("Вчера в %02d:%02d", hour, minute));
            } else if (year == calMy.get(Calendar.YEAR) && month == calMy.get(Calendar.MONTH) && calMy.get(Calendar.DAY_OF_MONTH) - day == 2) {
                viewHolder.tvDateCreated.setText(String.format("Позавчера в %02d:%02d", hour, minute));
            } else if (year == calMy.get(Calendar.YEAR)) {
                viewHolder.tvDateCreated.setText(String.format("%d %s в %02d:%02d", day, monthShort[month], hour, minute));
            } else {

                String dat = String.format("%d.%02d.%d", day, month, year);
                viewHolder.tvDateCreated.setText(dat);
            }


        } else {
            viewHolder.tvDateCreated.setText("Дата неизвестна");
        }
        String fN = record.getFileNameFULL();
        String anV = fN.substring(fN.indexOf("MK"), fN.indexOf("-"+sp.getString("devCode", "klteduos")));
        String vvv = anV.substring(2, anV.length());

        String mainV = vvv.substring(0, 1);
        String secV = vvv.substring(1, 2);
        String thrV = vvv.substring(3, 4);


        viewHolder.tvAndroidVersion.setText(String.format("Android %s.%s.%s", mainV, secV, thrV));


        if (record.getFirmwareType().contains("nightly")) {
            viewHolder.tvFirmwareType.setText("Ночная");
        } else if (record.getFirmwareType().contains("release")) {
            viewHolder.tvFirmwareType.setText("Релиз");
        } else {
            viewHolder.tvFirmwareType.setText("Неизвестная сборка");
        }

        viewHolder.tvFullSize.setText(String.format("Размер %s", record.getFileSize()));
        viewHolder.tvDownloadCount.setText(String.format("Скачано %s раз", record.getDownloadCount()));
        viewHolder.tvFileNameFull.setText(String.format("%s", record.getFileNameFULL()));

        String fileName = record.getFileNameFULL().substring(0, record.getFileNameFULL().indexOf(".zip"));
        Log.e("fileNameRecord = ", fileName);

        if (fileName.contains(platformVersion)) {
            //viewHolder.cvMain.setBackgroundColor(Color.parseColor("#6900BB00"));


            final SpannableStringBuilder sb = new SpannableStringBuilder(viewHolder.tvFileNameFull.getText().toString()+ "\nВаша текущая версия");

            // Span to set text color to some RGB value
            final ForegroundColorSpan fcs = new ForegroundColorSpan(Color.RED);

            // Span to make text bold
            final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD);

            // Set the text color for first 4 characters
            sb.setSpan(fcs, viewHolder.tvFileNameFull.getText().toString().length(), sb.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);

            // make them also bold
            sb.setSpan(bss, viewHolder.tvFileNameFull.getText().toString().length(), sb.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);

            viewHolder.tvFileNameFull.setText(sb);

            viewHolder.btnOTA.setVisibility(View.GONE);
        } else {
            viewHolder.btnOTA.setVisibility(View.VISIBLE);
        }


        String fileFull = record.getDownloadFull().substring(record.getDownloadFull().indexOf("get=")+4);
        Log.e("fileFull = ", fileFull);
        record.setDownloadFull("http://rom.mk/"+fileFull);

        viewHolder.btnFull.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) listener.onDownloadFullClick(record.getDownloadFull(), record.getFileNameFULL());
            }
        });

        viewHolder.btnOTA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) listener.onDownloadOTAClick("http://download.mokeedev.com/"+record.getDownloadOTA(), record.getFileNameFULL(), record.getFirmwareType().toUpperCase());
            }
        });

        final Date finalConvertedDate = convertedDate;
        viewHolder.btnChageLog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar cal =Calendar.getInstance(Locale.US);
                cal.setTime(finalConvertedDate);

                DateFormat df = new SimpleDateFormat("yy"); // Just the year, with 2 digits
                String formattedYear = df.format(cal.getTime());

                String formattedMonth = String.format("%02d",cal.get(Calendar.MONTH)+1);
                String formattedDay = String.format("%02d",cal.get(Calendar.DAY_OF_MONTH));

                String version = record.getFileNameFULL().substring(0, record.getFileNameFULL().indexOf("-"+sp.getString("devCode", "klteduos")));
                version = version.substring(0, version.lastIndexOf("."));

                String resultUrl = "http://changelog.mokeedev.com/read.php?change_log=Changelog_"+formattedYear+formattedMonth+formattedDay+".log&mk_version="+version;

                Log.e("ChangeLogURL", resultUrl);

                new loadChageLog().execute(resultUrl);

            }
        });


    }

    private class loadChageLog extends AsyncTask<String, Void, String>{

        Document doc = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {

            for (String url: params){

                try {
                    doc = Jsoup.connect(url).get();
                } catch (IOException e) {
                    e.printStackTrace();
                }


            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            if (doc != null){
                Elements el = doc.getElementsByClass("container");
                AlertDialog.Builder adb = new AlertDialog.Builder(ctx);
                adb.setTitle("История измнений");
                adb.setMessage(Html.fromHtml(el.toString()));
                adb.setPositiveButton("Закрыть", null);
                adb.show();
            }

        }
    }


    private boolean isEven(int n) {
        return (n & 1) == 0;
    }


    @Override
    public int getItemCount() {
        return records.size();
    }

    /**
     * Реализация класса ViewHolder, хранящего ссылки на виджеты.
     */
    class ViewHolder extends RecyclerView.ViewHolder {
        private CardView cvMain;
        private LinearLayout llMain;
        private TextView tvFirmwareType;
        private TextView tvAndroidVersion;
        private TextView tvFullSize;
        private TextView tvDownloadCount;
        private TextView tvDateCreated;
        private TextView tvFileNameFull;

        private Button btnFull;
        private Button btnOTA;
        private Button btnChageLog;


        public ViewHolder(View itemView) {
            super(itemView);
            cvMain = (CardView) itemView.findViewById(R.id.cvMain);
            llMain = (LinearLayout) itemView.findViewById(R.id.llMain);
            tvFirmwareType = (TextView) itemView.findViewById(R.id.firmwareType);
            tvAndroidVersion = (TextView) itemView.findViewById(R.id.androidVersion);
            tvFullSize = (TextView) itemView.findViewById(R.id.fileFULLSIZE);
            tvDownloadCount = (TextView) itemView.findViewById(R.id.downloadCountFull);
            tvDateCreated = (TextView) itemView.findViewById(R.id.createDate);
            tvFileNameFull = (TextView) itemView.findViewById(R.id.fileName);
            btnFull = (Button) itemView.findViewById(R.id.btnFULL);
            btnOTA = (Button) itemView.findViewById(R.id.btnOTA);
            btnChageLog = (Button) itemView.findViewById(R.id.btnChangeLog);
        }
    }


}
